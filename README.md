# cordaria

## O que é?
Destinado à pessoas interessadas em iniciar o estudo do violão e guitarra e professores dessa arte, o projeto Cordaria é um aplicativo web com intuito de auxiliar a prática ao instrumento.

Desenvolvido pelo músico e programador Lu Sacramento, o projeto é fruto de uma pesquisa sobre novas metodologias e abordagens ao ensino à distância de música.

## O que Faz?

Sua função é gerar exercícios básicos de digitação com foco na técnica. Tais exercícios vem para auxiliar o educando a executar as notas dadas através de uma escrita de tablatura, cuja a linguagem é de fácil entendimento comparado a escrita de partitura.

Desta forma, o aluno poderá praticar junto ao auxílio de um “correpetidor” virtual, em qualquer momento e lugar!

## A Pesquisa
A pesquisa “Cordaria – Desenvolvimento de aplicação web para iniciação à prática do violão e guitarra” surge em um momento delicado da história mundial: a pandemia do Coronavírus – COVID 19. Compelidos a este problema, os profissionais da área da educação, sobre tudo com atuação na área da música enfrentam grandes desafios para conseguir adequar suas atividades para o ensino a distância. Parte deste desafio é devido a escassez de recursos tecnológicos que fogem da metodologia tradicional de ensino, muito das vezes, limitado a vídeos, textos, imagens e de forma repetida. Os problemas se amplificam com usuários oriundos da classe baixa, que tem uma limitação considerável de hardware e/ou baixa conexão de internet.

Utilizando meus conhecimentos de uma década como professor e alguns anos de desenvolvedor web, implementei uma aplicação para prática de exercícios básicos de violão e guitarra baseada em alguns métodos já experienciados por mim enquanto discente e docente. Com isso, Cordaria pretende ampliar as possibilidades metodológicas do ensino prático destes instrumentos num cenário EAD e oferecer uma oportunidade diferente para iniciação da arte do fazer musical, além de incentivar programadores e professores a pensar novas formas de aprendizagem.

## Equipe
Developer: Lu Sacramento
Designer: Gabriel Barreto

# Créditos
Esta pesquisa foi possível devido ao apoio da Lei Emergencial de Incentivo à Cultura – Lei Aldir Blanc, Governo Federal, Ministério do Turismo e Secretaria Especial de Cultura, através da aprovação no Edital LAB no 14/2020 – “Seleção de Bolsistas para as Áreas Artísticas Técnicas e de Produção Cultural” promovido pela Secretaria de Cultura e Turismo de Minas Gerais – SECULT.

![Patrocínio](https://cordaria.com.br/_nuxt/img/public-agencies-horizontal-logo.fac1f52.png)

## Rode sem a necessidade de instalar
Acesse -> [https://cordaria.com.br](https://cordaria.com.br) <-

## Build Setup (Instruções para compilar. Documentação em inglês)

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

## Special Directories

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

### `assets`

The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).


### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

### `store`

This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).
